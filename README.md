# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version: 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This project requires IDE such as Visual Studio Code
* Install node.js, mongoDB and Express modules
* Install dependencies for mongoDB, npm
* Configure Database for MongoDB
* Run the index.js in a browser for testing the functionality
* Deploy the website

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### License ###

* The license selected is GNU General Public License.
* It is a free, copyleft license for software and other kinds of works.
* The licenses for most software and other practical works are designed to take away your freedom to share and change the works.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact