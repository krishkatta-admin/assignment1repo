const express = require ('express');
const path = require ('path');
const {check, validationResult} = require('express-validator');
const { RSA_PSS_SALTLEN_DIGEST } = require('constants');
const fileUpload = require('express-fileupload');

// Get Express Session
const session = require('express-session');

//DB Connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/finalproject ',{
    useNewUrlParser : true,
    useUnifiedTopology : true
});


var myApp = express ();
myApp.use (express. urlencoded({extended:true}));

myApp.set ('views', path.join (__dirname, 'views'));
myApp.use (express.static(__dirname + '/public'));
myApp.set ('view engine', 'ejs');
myApp.use(fileUpload());

// Setup Session
myApp.use(session({
    secret : 'superrandomsecret',
    resave : false,
    saveUninitialized : true
}));

myApp.use(function (req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

//Setup model for the collection - admin
const Admin = mongoose.model('Admin', {
    username : String,
    password : String
});

// define the collection(s)
const Createpost = mongoose.model('Createpost',{
    postTitle: String,
    postImageName: String,
    postDescription: String
});

//home page 
myApp.get ('/', function (req, res)
{
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('home', {createposts:createposts, userLoggedIn: req.session.userLoggedIn}); 
    });
});


//about us page
myApp.get('/author', function (req,res){
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('author', {createposts:createposts, userLoggedIn: req.session.userLoggedIn,name : "Krishna Katta",studentNumber : '8752129'}); 
    });
});

//Login
myApp.get('/login', function(req, res){

    Createpost.find({}).exec(function(err, createposts){ 
        res.render('login', {createposts:createposts, userLoggedIn: req.session.userLoggedIn}); 
    });
});

myApp.post('/login', function(req,res){
    var user = req.body.username;
    var pass = req.body.password;    
    
    Admin.findOne({username : user, password : pass}).exec(function(err, admin){
        console.log('Error: ' + err);
        console.log('Admin: ' + admin);

        if (admin)
        {
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            res.redirect('/createpost');
        }
        else 
        {
            res.render('login', {error : "Sorry Login Failed!"});
        }
    });

});

myApp.get('/createpost',function(req, res){
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('createpost', {createposts:createposts, userLoggedIn: req.session.userLoggedIn}); 
    });
});

myApp.post('/create',[
    check('postTitle', 'Please enter a Title.').not().isEmpty(),
    check('postDescription', 'Please enter a Description').not().isEmpty()
], function(req,res){
  Createpost.find({}).exec(function(err, createposts){ 
    // check for errors
    const errors = validationResult(req);
    console.log(errors);
    if(!errors.isEmpty())
    {
        res.render('createpost',{userLoggedIn: req.session.userLoggedIn,er: errors.array()});
    }
    else
    {
        var postTitle = req.body.postTitle;
        var postDescription = req.body.postDescription;

        // get the name of the file
        var postImageName = req.files.postImage.name;
        // get the actual file (temporary file)
        var postImageFile = req.files.postImage;
        
        var postImagePath = 'public/uploads/' + postImageName;
        // move temp file to the correct folder (public folder)
        postImageFile.mv(postImagePath, function(err){
            console.log(err);
        });

        var pageData = {
            postTitle : postTitle,
            postDescription : postDescription,
            postImageName: postImageName
        }

        // save data to database
        var myPost = new Createpost(pageData);

        myPost.save().then(function(){
            console.log("New post created!");
        });

        // send the data to the view and render it
        res.render('displaypost', {
            postTitle : postTitle,
            postDescription : postDescription,
            postImageName: postImageName,
            createposts: createposts // pass the post for menu
            ,userLoggedIn: req.session.userLoggedIn
        });
    }
  });
});

// for fetching one single card and displaying
myApp.get('/display/:createpostid', function(req,res){ 
        var createpostID = req.params.createpostid;
        console.log(createpostID);
        Createpost.findOne({_id: createpostID}).exec(function(err,createpost){
            console.log('Error: ' + err);
            console.log('Post found: ' + createpost);

            if(createpost){
                res.render('displaypost', {
                    postTitle : createpost.postTitle,
                    postDescription : createpost.postDescription,
                    postImageName: createpost.postImageName,
                    createposts: createpost.createposts,
                    userLoggedIn: req.session.userLoggedIn
                });
            }
            else{
                res.status(404).send('404: Sorry page not found.');
            }
        });
    // });
});

// All Post Page
myApp.get('/displayallpost', function(req,res){
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('displayallpost', {createposts:createposts, userLoggedIn: req.session.userLoggedIn}); 
    });
});

//Delete Page
myApp.get('/delete/:id', function (req,res){
    if (req.session.username) {
        // Delete
        var objid = req.params.id;
        Createpost.findByIdAndDelete({_id : objid}).exec(function(err, createpost){
            console.log("Error: " + err);
            console.log("Order: " + createpost);
            if (createpost) {
                res.render('delete', {message : "Successfully Deleted...!!", userLoggedIn: req.session.userLoggedIn});
            }
            else {
                res.render('delete', {message : "Sorry, record not deleted...!!", userLoggedIn: req.session.userLoggedIn});
            }
        });
    }
    else {
        res.redirect('/login');
    }
});

// Edit Page - Get Method
myApp.get('/edit/:id', function (req,res){
    if (req.session.username) {
        // Edit
        var objid = req.params.id;
        Createpost.findOne({_id : objid}).exec(function(err, createpost){
            console.log("Error: " + err);
            console.log("Createpost: " + createpost);
            if (createpost) {
                res.render('edit', {createpost : createpost, userLoggedIn: req.session.userLoggedIn});
            }
            else {
                res.send ('No post found with this id.');
            }
        });
    }
    else {
        res.redirect('/login');
    }
});

// Edit Page - Post Method
myApp.post('/edit/:id', [
    check('postTitle', 'Please enter a Title.').not().isEmpty(),
    check('postDescription', 'Please enter a Description').not().isEmpty()

], function (req, res) {
    
    const errors = validationResult (req);
    
    console.log(errors); 

    if (!errors.isEmpty())
    {
        var id = req.params.id;
        Createpost.findOne({_id : id}).exec(function(err, createpost){
            console.log("Error: " + err);
            console.log("Createpost: " + createpost);
            if (createpost) {
                res.render('edit', {createpost : createpost, errors : errors.array(), userLoggedIn: req.session.userLoggedIn});
            }
            else {
                res.send ('No order found with this id.');
            }
                   
        });
    }
    else 
    {
        var postTitle = req.body.postTitle;
        var postDescription = req.body.postDescription;

        // get the name of the file
        var postImageName = req.files.postImage.name;
        // get the actual file (temporary file)
        var postImageFile = req.files.postImage;
        
        var postImagePath = 'public/uploads/' + postImageName;
        // move temp file to the correct folder (public folder)
        postImageFile.mv(postImagePath, function(err){
            console.log(err);
        });

        var pageData = {
            postTitle : postTitle,
            postDescription : postDescription,
            postImageName: postImageName
        }

        var id = req.params.id;
        Createpost.findOne({_id: id}). exec(function(err, createpost){
            createpost.postTitle = postTitle;
            createpost.postDescription = postDescription;
            createpost.postImageName = postImageName;
            createpost.save();
        });

        // Display Output
        res.render ('editsuccess', {pageData, userLoggedIn: req.session.userLoggedIn});
    }
});


//home page 
myApp.get ('/', function (req, res)
{
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('home', {createposts:createposts, userLoggedIn: req.session.userLoggedIn}); 
    });

    res.render('home', {error : "Sorry Login Failed!"});
});


//about us page
myApp.get('/author', function (req,res){
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('author', {createposts:createposts, userLoggedIn: req.session.userLoggedIn,name : "Krishna Katta",studentNumber : '8752129'}); 
    });

    //res.render('author', {name : "Krishna Katta",studentNumber : '8752129'});
});

//Logout Page
myApp.get('/logout', function (req,res){
    req.session.username = '';
    req.session.userLoggedIn = false;
    Createpost.find({}).exec(function(err, createposts){ 
        res.render('login', {createposts:createposts, userLoggedIn: req.session.userLoggedIn,error : "Successfully logged out!"}); 
    });
});

myApp.listen(8010); 
console.log('Website opened at port 8010!');
